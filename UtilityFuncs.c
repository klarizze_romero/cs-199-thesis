#include "InputDefs.h";
#include "DNACommonFuncs.h";
#include "UtilityFuncs.h";


void PrintInputString(const CInputString * inputStr, const char isNumber, FILE * f) {	//prints DNA string
	fprintf(f, "len=%d\n", inputStr->m_length);	
	PrintDNAString(inputStr->m_data, inputStr->m_length, 1, isNumber, f);
}

void AddReverseComplementStrsIntoInputStrs(CInputStringSet * inputStrs) {//??? but it is not used
	int i, j;
	int maxLen, curLen;
	for (i = 1; i < inputStrs->m_num; i++) {
		curLen = inputStrs->m_str[i].m_length;
		maxLen = curLen * 2;
		if (maxLen > CONST_MAX_INPUT_STRING_LENGTH) {
			maxLen = CONST_MAX_INPUT_STRING_LENGTH;
		}
		for (j = 0; j < maxLen - curLen; j++) {
			inputStrs->m_str[i].m_data[curLen + j] = 3 - inputStrs->m_str[i].m_data[curLen - j - 1];	
		}
		inputStrs->m_str[i].m_length = maxLen;
	}
}

int CmpTwoStrsBasedOnLength(CInputString * str1, CInputString * str2) {
	return (str1->m_length - str2->m_length);	
}// if value >0, str1->m_lenth> str2->m_length. 
//else if value ==0, equal sila. else, str2>str1

void SortStrsOfInputStrs(CInputStringSet * inputStrs) {
	qsort(inputStrs->m_str, inputStrs->m_num, sizeof(CInputString), CmpTwoStrsBasedOnLength);
}//sorts number converted dna sequences by length shortest to longest

void ReadInputFile(const char * fName, CInputStringSet * inputStrs) {
	
	char str[1000000];
	FILE * f;
	
	int i, tempLen;

	inputStrs->m_num = -1;

	f = fopen(fName, "r");
	if (f == NULL) {//if file doesn't exists 
		return;
	}

	//read each line
	while ( fgets(str, sizeof(str), f) && inputStrs->m_num < CONST_MAX_NUM_STRINGS) {	
		if (str[0] != '>' && strlen(str) > 0 && inputStrs->m_num >= 0) {//not yet > so reading characters in a sequence pa
			if (inputStrs->m_str[inputStrs->m_num].m_length >= CONST_MAX_INPUT_STRING_LENGTH) {//when max length is reached, it will go back at the top o the loop
				continue;
			}
			str[strlen(str) - 1] = '\0';//puts null at the end of the string because maniplating string in c is hard
			tempLen = CONST_MAX_INPUT_STRING_LENGTH - inputStrs->m_str[inputStrs->m_num].m_length;//max_input_string_length - length of input sequence
			if ( tempLen > strlen(str) ) {
				tempLen = strlen(str);
			}
			memcpy(inputStrs->m_str[inputStrs->m_num].m_data + inputStrs->m_str[inputStrs->m_num].m_length, str, tempLen);//copies string to variable
			inputStrs->m_str[inputStrs->m_num].m_length += tempLen;//increment length		
		} else {//neww sequence
			inputStrs->m_num++;//new sequence number
			inputStrs->m_str[inputStrs->m_num].m_length = 0;//initialize length
		}
	}
	inputStrs->m_num++;//increment last sequence
	fclose(f);

	for ( i = 0; i < inputStrs->m_num; i++) {
		MapDNAStringFromLetterToNumber(inputStrs->m_str[i].m_data, inputStrs->m_str[i].m_length);//each dna sequences are converted into numbers	
	}
	//AddReverseComplementStrsIntoInputStrs(inputStrs); //purpose not disclosed 
	SortStrsOfInputStrs(inputStrs);//sorts number converted dna sequences by length shortest to longest
}

void PrintInputStrSet2File(FILE * f, const CInputStringSet * inputStrs, int startSeq, int endSeq) {
	int i;
	for (i = startSeq; i <= endSeq; i++ ) {
		fprintf(f, ">Sequence %d\n", i + 1);		//print sequence no. 
		PrintDNAString(inputStrs->m_str[i].m_data, inputStrs->m_str[i].m_length, 1, 0, f);/*(const char * str, const  int len, 
					const char strMode, const char printMode, FILE * f) if printMode==0, print the numbers into DNA*/
		fprintf(f, "\n");
	}
}

/*
int memcmp(const void *str1, const void *str2, size_t n) from http://www.tutorialspoint.com/c_standard_library/c_function_memcmp.htm
str1 -- This is the pointer to block of memory.

str2 -- This is the pointer to block of memory.

n -- This is the number of bytes to be compared*/
int CompareTwoCompactMotifs(const CCompactMotif * m1, const CCompactMotif * m2) {
	return memcmp((char *)m1, (char *)m2, sizeof(CCompactMotif));
}
unsigned int RemoveDupCompMotifsInOrderArray(CCompactMotif * motifs, 
											   const unsigned int numMotifs) {//compares motifs that are converted into bytes 
	CCompactMotif * ptr1 = motifs; 
	CCompactMotif * ptr2 = motifs + 1;//2nd motif
	CCompactMotif * ptrE = motifs + numMotifs;
	if (numMotifs == 0) {
		return 0;
	}
	for (; ptr2 < ptrE; ptr2++) {
		if ( memcmp(ptr1, ptr2, sizeof(CCompactMotif)) != 0 ) {//if motifs found are not equal
			ptr1++;//next motif to see if there are duplicates
			if (ptr1 < ptr2) {//ptr1=ptr2 
				memcpy(ptr1, ptr2, sizeof(CCompactMotif));
			}

		}
	}
	return (ptr1 - motifs + 1);
}
int CompareTwoMotifs(const CMotif * m1, const CMotif * m2) {//compares two motifs (not in bytes) in terms of bytes???
	return memcmp((char *)m1, (char *)m2, sizeof(CMotif)) ;
}
unsigned int RemoveDupMotifsInOrderArray(CMotif * motifs, const unsigned int numMotifs) {//removes duplicate by removing same motifs (not used?)
	CMotif * ptr1 = motifs; 
	CMotif * ptr2 = motifs + 1;//2nd motif
	CMotif * ptrE = motifs + numMotifs;
	if (numMotifs == 0) {
		return 0;
	}
	for (; ptr2 < ptrE; ptr2++) {
		if ( memcmp(ptr1, ptr2, sizeof(CMotif)) != 0 ) {
			ptr1++;
			if (ptr1 < ptr2) {
				memcpy(ptr1, ptr2, sizeof(CMotif));
			}

		}
	}
	return (ptr1 - motifs + 1);
}

int HammingDistStrStr(const char * str1, const char * str2, const  int len) {	//computes hamming distance 
	int dist = 0;
	const char * ptrE = str1 + len;
	for (; str1 < ptrE; str1++) {
		if ( !(*str1 == *str2) ) {
			dist++;
		}
		str2++;
	}
	return dist;
}
int HammingDistStrInputStrAtPos(const char * str,  int strLen, 
										 const CInputString * inputStr, 
										 const  int pos) {// pos is the starting position
	int dist = 0;
	const char * ptrInput = inputStr->m_data + pos;//pos in m_data
	const char * strE = str + strLen;
	for (; str < strE ; str++) {//compare the motif with the string in part of a sequence
		if ( !(*str == *ptrInput) ) {//if not equal
			dist++;//incement mismatches 
		}
		ptrInput++;//next position
	}
	return dist;
}
int HammingDistStrInputStr(const char * str,  int strLen,
									const CInputString * inputStr) {	//minimum hamming distance of the motif and the sequence
	 int i;
	 int tempDist, dist = strLen;
	 int numLoc = inputStr->m_length - strLen;
	for (i = 0; i < numLoc; i++) {
		tempDist = HammingDistStrInputStrAtPos(str, strLen, inputStr, i);		
		if ( dist > tempDist ) {
			dist = tempDist;
		}
	}
	return dist;
}

int AtPosMotifInputStr(const char * motif, 
					 const int mLen, const int hd,
					 const CInputString * inputStr) {	// find a binding site in a sequence 
	int i, len = inputStr->m_length - mLen;
	for (i = 0; i < len; i++) {				
		if ( HammingDistStrInputStrAtPos(motif, mLen, inputStr, i) <= hd ) {
			return i;
		}
	}
	return i;
}
char IsMotifInputStr(const char * motif, 
					 const int mLen, const int hd,
					 const CInputString * inputStr) {	// see if motif is in the sequence 
	int i, len = inputStr->m_length - mLen;
	for (i = 0; i < len; i++) {//see all the possible positions the motif can be in a sequence				
		if ( HammingDistStrInputStrAtPos(motif, mLen, inputStr, i) <= hd ) {// at the postion where the motif is found, see if the hamming distance is less than the max
			return 1;
		}
	}
	return 0;
}

char IsMotifInputStrSet(const char * motif, 
						const  int mLen, const  int hd,
					    const CInputStringSet * inputStrSet, 
						const int loStr, const int hiStr) {	
	int i;
	for (i = hiStr; i >= loStr; i--) {//see if motif is in all dna sequences besides where it is found initially				
		if ( !IsMotifInputStr(motif, mLen, hd, &inputStrSet->m_str[i]) ) {
			return 0;//fail
		}
	}
	return 1;//yes, it does
}
char IsMotifInputStrSetWithBindingSite(const char * motif, 
						const  int mLen, const  int hd,
					    const CInputStringSet * inputStrSet, 
						const int loStr, const int hiStr) {	//is motif considering bonding site of string set
	int i;
	char isMotif = 1;
	for (i = hiStr; i >= loStr; i--) {				
		if ( !IsMotifInputStr(motif, mLen, hd, &inputStrSet->m_str[i]) ) {
			//return 0;
			isMotif = 0;
		}
	}
	fprintf(stdout, "\n");
	for (i = loStr; i <= hiStr; i++) {				
		fprintf(stdout, "%d ", AtPosMotifInputStr(motif, mLen, hd, &inputStrSet->m_str[i]));
	}
	return isMotif;// 0 if not. 1 if yes. motif is in the binding site
}

int MaxHammingDistFrom1StrToNStrs( const char * str, 											
								   const char ** strs, 
								   const int numStrs, 
								   const int strLen) {//gets the max hamming distance of a string (str) in all sequences
	int tempDist, dist = 0;
	const char ** strsE = strs + numStrs;
	for (; strs < strsE; strs++) {
		tempDist = HammingDistStrStr(str, (*strs), strLen);
		if ( dist < tempDist ) {//gets bigger distance
			dist = tempDist;
		}		
	}
	return dist;
}

