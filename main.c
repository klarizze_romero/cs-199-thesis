#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include "InputDefs.h"
#include "DNACommonFuncs.h"
#include "BitFuncs.h"
#include "SortFuncs.h"
#include "UtilityFuncs.h"
#include "PMSPrune.h"


//global input parameters
int motifLen = 11;//motif length
int hammingDist = 3;//hamming distance
CInputStringSet inputStrs;//store all set ofinput sequcness
CCompactMotif foundMotifs[CONST_MAX_NUM_FOUND_MOTIFS_ALLOWED];//stores motifs in their byte form 
int numFoundMotifs = 0;//found motifs

int VerifyMotifs( int moLen, int hmDist, const CInputStringSet * inptStrs,
				  const CCompactMotif * foMotifs, int nFoMotifs) {
	int i;
	int nCorrectMotifs = 0;
	char aMotif[CONST_MAX_MOTIF_STRING_LENGTH + 4];
	for (i = 0; i < nFoMotifs; i++) {
		DecodeDNAString((char *)&foMotifs[i], moLen/4 + 1, aMotif);//decodes encoded motifs
		//fprintf(stdout, "\nTest: ");
		//PrintDNAString(aMotif, moLen, 1, 0, stdout);
		if (IsMotifInputStrSet(aMotif, moLen, hmDist, inptStrs, 0, inptStrs->m_num - 1)) {
		//if (IsMotifInputStrSetWithBindingSite(aMotif, moLen, hmDist, inptStrs, 0, inptStrs->m_num - 1)) {//????
			nCorrectMotifs++;//if motif appears in all sequences, increment
		} else {

		}
	}
	return nCorrectMotifs;
}


void PrintMotitsToFile(const char * fName, int moLen, CCompactMotif * foMotifs, int nMotifs) {//prints motifs found to file
	int i;
	char aMotif[CONST_MAX_MOTIF_STRING_LENGTH];
	FILE * f;
	f = fopen(fName, "w+");
	if (f == NULL) {
		fprintf(stdout, "\nUnable to create output file %s", fName);
		return;
	}
	//fprintf(stdout, "#Motifs=%d\n", nMotifs);
	for (i = 0; i < nMotifs; i++) {
		DecodeDNAString(foMotifs + i, moLen/4 + 1, aMotif);		
		PrintDNAString(aMotif, moLen, 1, 0, f);//prints DNA sequences
		fprintf(f, "\n");
	}
	//fprintf(stdout, "#Motifs=%d", nMotifs);
	fclose(f);	
}

void main(int argc, char *argv[]) {

	int t1, t2;
	int i, j;	
	char iFileName[500];
	char oFileName[500];

	ComputeDNAByteTable();//fixed computes all possible dna combinations in bytes
	ComputeDNAByteTableInverse();//inverse of the DNAByteTable


	motifLen = 13;
	hammingDist = 4;
	sprintf(iFileName, "TestingInput\\input_l%d_d%d_0.txt", motifLen, hammingDist);
	sprintf(oFileName, "output_l%d_d%d_0.txt", motifLen, hammingDist);
		
	if (argc < 5) {
		fprintf(stdout, "\nWrong parameters.\nCommand line: \nPMSPrune.exe [input file] [output file] [motif length] [d]");
		return;
	} else {
		strcpy(iFileName, argv[1]);
		strcpy(oFileName, argv[2]);
		motifLen = atoi(argv[3]);
		hammingDist = atoi(argv[4]);		
	}

	ReadInputFile(iFileName, &inputStrs);//read input file

	fprintf(stdout, "\n\nInput Strings: %d strings l=%d d=%d", inputStrs.m_num, motifLen, hammingDist);
	for (i = 0; i < inputStrs.m_num; i++) {	
		fprintf(stdout, "\n\n%d\n", i + 1);
		PrintInputString(&inputStrs.m_str[i], 0, stdout);//will change this//prints number to DNA
	}

	t1 = time(NULL);//starting time
	numFoundMotifs = PMSPrune(motifLen, hammingDist, &inputStrs, foundMotifs, CONST_MAX_NUM_FOUND_MOTIFS_ALLOWED);
	t2 = time(NULL);//ending time
	fprintf(stdout, "\nTime=%d seconds", t2 - t1);//t2-t1 is running time

	PrintMotitsToFile(oFileName, motifLen, foundMotifs, numFoundMotifs);//prints all motifs found to file

	return;
}




